package pagamento;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conta.SqlConta;

@WebServlet(name="Pagamento", urlPatterns = {"/pagamento"})
public class SrvPagamento extends HttpServlet{

	private static final long serialVersionUID = 9L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		String[] checks = req.getParameterValues("checkbox");
		SqlPagamento sql = new SqlPagamento();
		if(req.getParameter("id") != null && !req.getParameter("id").equals("undefined")){
			RequestDispatcher view = req.getRequestDispatcher("/pagamento/cadastroPagamento.jsp");
			view.forward(req, resp);
		}else{
			for(String check :checks){
				try {
					sql.delete(Integer.parseInt(check));
					
				} catch (NumberFormatException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					resp.getWriter().println("N�o Deu");
				}
				
			}
			RequestDispatcher view = req.getRequestDispatcher("/pagamento/pagamento.jsp");
			view.forward(req, resp);
		}
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/pagamento/pagamento.jsp");
		view.forward(req, resp);
		
	}
	
	
}
