package pagamento;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import categoria.Categoria;
import categoria.SqlCategoria;

@WebServlet(name="CadastroPagamento", urlPatterns = {"/cadastroPagamento"})
public class SrvCadastroPagamento extends HttpServlet{

	private static final long serialVersionUID = 8L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		Pagamento pagamento = new Pagamento();
		SqlPagamento sql = new SqlPagamento();
		
		pagamento.setTipo(req.getParameter("formaPagamento"));
		try {
			if(req.getParameter("id") != null && req.getParameter("id") != ""){
				pagamento.setId(Integer.parseInt(req.getParameter("id")));
				sql.update(pagamento);
				resp.sendRedirect("./pagamento");
			}else{
				sql.insert(pagamento);
				resp.sendRedirect("./pagamento");
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.getWriter().println("N�o Deu");
		}
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/pagamento/cadastroPagamento.jsp");
		view.forward(req, resp);
		
	}
	
	
}
