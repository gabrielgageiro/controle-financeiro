package pagamento;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import banco.Conecta;
import categoria.Categoria;

public class SqlPagamento {

	public SqlPagamento() {
		try {
			createTable();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createTable() throws ClassNotFoundException{		
		final String sql = "CREATE TABLE IF NOT EXISTS 'pagamento' ('id'	INTEGER PRIMARY KEY AUTOINCREMENT,'nome'	TEXT NOT NULL);";
		try (Connection conn = Conecta.conectar();
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}
	
	public void insert(Pagamento pagamento) throws ClassNotFoundException{
		final String sql = "INSERT INTO pagamento VALUES(?,?)";
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);) {
        	ps.setString(1, null);
        	ps.setString(2, pagamento.getTipo());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public List<Pagamento> selctAll() throws ClassNotFoundException{
		final String sql = "SELECT id, nome FROM pagamento";
        List<Pagamento> pagamentos = new ArrayList<>();
        try (Connection conn = Conecta.conectar();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
            	Pagamento pagamento = new Pagamento(rs.getInt("id"), rs.getString("nome"));
                pagamentos.add(pagamento);
            }
            stmt.close();
            rs.close();
            conn.close();
            return pagamentos;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}
	
	public Pagamento selectItem(int id) throws ClassNotFoundException{
		String sql = "SELECT id, nome FROM pagamento WHERE id == ?";
		Pagamento pagamento = new Pagamento();
		try (Connection conn = Conecta.conectar();PreparedStatement pstmt  = conn.prepareStatement(sql)){
			pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
			while (rs.next()) {
				pagamento.setId(rs.getInt("id"));
				pagamento.setTipo(rs.getString("nome"));
			}
			pstmt.close();
            rs.close();
			conn.close();
			return pagamento;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public void delete(int id) throws ClassNotFoundException{		
		final String sql = "DELETE FROM pagamento WHERE id = ?";		 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){
            ps.setInt(1, id);
            ps.executeUpdate(); 
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public void update(Pagamento pagamento) throws ClassNotFoundException{		
		final String sql = "UPDATE pagamento SET nome = ? WHERE id = ?";
 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){ 
            ps.setString(1, pagamento.getTipo());
            ps.setInt(2, pagamento.getId());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
}
