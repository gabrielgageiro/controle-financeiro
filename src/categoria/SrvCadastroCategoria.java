package categoria;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="CadastroCategoria", urlPatterns = {"/cadastroCategoria"})
public class SrvCadastroCategoria extends HttpServlet{
	
	private static final long serialVersionUID = 2L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		Categoria categoria = new Categoria();
		SqlCategoria sql = new SqlCategoria();
		
		categoria.setNome(req.getParameter("categoria"));
		try {
			if(req.getParameter("id") != null && req.getParameter("id") != ""){
				categoria.setId(Integer.parseInt(req.getParameter("id")));
				sql.update(categoria);
				resp.sendRedirect("./categoria");
			}else{
				sql.insert(categoria);
				resp.sendRedirect("./categoria");
			}			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.getWriter().println("N�o Deu");
		}



	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{	
		RequestDispatcher view = req.getRequestDispatcher("/categoria/cadastroCategoria.jsp");
		view.forward(req, resp);
		
	}
	
	
}
