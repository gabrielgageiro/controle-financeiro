package categoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import banco.Conecta;

public class SqlCategoria {
	
	public SqlCategoria() {
		try {
			createTable();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
    
	public void createTable() throws ClassNotFoundException{		
		final String sql = "CREATE TABLE IF NOT EXISTS 'categoria' ('id'	INTEGER PRIMARY KEY AUTOINCREMENT,'nome'	TEXT NOT NULL);";
		try (Connection conn = Conecta.conectar();
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}
	
	public void insert(Categoria categoria) throws ClassNotFoundException{
		final String sql = "INSERT INTO categoria VALUES(?,?)";
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);) {
        	ps.setString(1, null);
        	ps.setString(2, categoria.getNome());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public List<Categoria> selctAll() throws ClassNotFoundException{
		final String sql = "SELECT id, nome FROM categoria";
        List<Categoria> categorias = new ArrayList<>();
        try (Connection conn = Conecta.conectar();Statement stmt  = conn.createStatement();ResultSet rs = stmt.executeQuery(sql)){
            while (rs.next()) {
            	Categoria categoria = new Categoria(rs.getInt("id"), rs.getString("nome"));
                categorias.add(categoria);
            }
            stmt.close();
            rs.close();
            conn.close();
            return categorias;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}
	

	
	public Categoria selectItem(int id) throws ClassNotFoundException{
		String sql = "SELECT id, nome FROM categoria WHERE id == ?";
		Categoria categoria = new Categoria();
		try (Connection conn = Conecta.conectar();PreparedStatement pstmt  = conn.prepareStatement(sql)){
			pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
			while (rs.next()) {
				categoria.setId(rs.getInt("id"));
				categoria.setNome(rs.getString("nome"));
			}
			pstmt.close();
            rs.close();
			conn.close();
			return categoria;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public void delete(int id) throws ClassNotFoundException{		
		final String sql = "DELETE FROM categoria WHERE id = ?";		 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){
            ps.setInt(1, id);
            ps.executeUpdate(); 
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public void update(Categoria categoria) throws ClassNotFoundException{		
		final String sql = "UPDATE categoria SET nome = ? WHERE id = ?";
 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){ 
            ps.setString(1, categoria.getNome());
            ps.setInt(2, categoria.getId());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}