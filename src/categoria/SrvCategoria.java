package categoria;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="Categoria", urlPatterns = {"/categoria"})
public class SrvCategoria extends HttpServlet{

	private static final long serialVersionUID = 3L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		String[] checks = req.getParameterValues("checkbox");
		SqlCategoria sql = new SqlCategoria();
		if(req.getParameter("id") != null && !req.getParameter("id").equals("undefined")){
			RequestDispatcher view = req.getRequestDispatcher("/categoria/cadastroCategoria.jsp");
			view.forward(req, resp);
		}else{
			for(String check :checks){
				try {
					sql.delete(Integer.parseInt(check));
					
				} catch (NumberFormatException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					resp.getWriter().println("N�o Deu");
				}
				
			}
			RequestDispatcher view = req.getRequestDispatcher("/categoria/categoria.jsp");
			view.forward(req, resp);
		}
		
		//resp.getWriter().println("Beleza");
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/categoria/categoria.jsp");
		view.forward(req, resp);
		
	}
	
	
}
