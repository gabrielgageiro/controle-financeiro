package banco;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conecta {
	private Connection conn = null;
	
	public static Connection conectar() throws ClassNotFoundException{
		Class.forName("org.sqlite.JDBC");
		Connection conn = null;
        try {
            String url = "jdbc:sqlite:banco.db"; 
            return DriverManager.getConnection(url);  
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
		return conn;
    }
	
	
}
