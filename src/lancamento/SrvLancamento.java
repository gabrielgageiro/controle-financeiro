package lancamento;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conta.SqlConta;

@WebServlet(name="Lancamento", urlPatterns = {"/lancamento"})
public class SrvLancamento extends HttpServlet{

	private static final long serialVersionUID = 7L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		String[] checks = req.getParameterValues("checkbox");
		SqlLancamento sql = new SqlLancamento();
		if(req.getParameter("id") != null && !req.getParameter("id").equals("undefined")){
			RequestDispatcher view = req.getRequestDispatcher("/lancamento/cadastroLancamento.jsp");
			view.forward(req, resp);
		}else{
			for(String check :checks){
				try {
					sql.delete(Integer.parseInt(check));
					
				} catch (NumberFormatException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					resp.getWriter().println("N�o Deu");
				}
				
			}
			RequestDispatcher view = req.getRequestDispatcher("/lancamento/lancamento.jsp");
			view.forward(req, resp);
		}
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/lancamento/lancamento.jsp");
		view.forward(req, resp);
		
	}
	
	
}
