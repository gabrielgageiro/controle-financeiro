package lancamento;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pagamento.SqlPagamento;
import utils.Tipo;
import categoria.SqlCategoria;
import conta.Conta;
import conta.SqlConta;

@WebServlet(name="CadastroLancamento", urlPatterns = {"/cadastroLancamento"})
public class SrvCadastroLancamento extends HttpServlet{

	private static final long serialVersionUID = 6L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		Lancamento lancamento = new Lancamento();
		SqlLancamento sql = new SqlLancamento();
		SqlCategoria sqlCat = new SqlCategoria();
		SqlPagamento sqlPag = new SqlPagamento();
		SqlConta sqlCont = new SqlConta();
		
		try {
			lancamento.setCategoria(sqlCat.selectItem(Integer.parseInt(req.getParameter("categoria"))));
			lancamento.setTipoPagamento(sqlPag.selectItem(Integer.parseInt(req.getParameter("tipoPagamento"))));
			lancamento.setConta(sqlCont.selectItem(Integer.parseInt(req.getParameter("conta"))));
		} catch (NumberFormatException | ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lancamento.setValor(new Double(req.getParameter("valor")));
		lancamento.setTipo(Tipo.valueOf(req.getParameter("tipo")));
		try {
			if(req.getParameter("id") != null && req.getParameter("id") != ""){
				lancamento.setId(Integer.parseInt(req.getParameter("id")));				
				sql.update(lancamento);
				resp.sendRedirect("./lancamento");
			}else{
				sql.insert(lancamento);
				resp.sendRedirect("./lancamento");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.getWriter().println("N�o Deu");
		}
		
	}
		
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/lancamento/cadastroLancamento.jsp");
		view.forward(req, resp);
		
	}
	
	
}
