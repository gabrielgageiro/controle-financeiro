package lancamento;
import conta.SqlConta;
import categoria.SqlCategoria;
import pagamento.Pagamento;
import pagamento.SqlPagamento;
import utils.Tipo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import banco.Conecta;

public class SqlLancamento {
	
	private SqlCategoria sqlCat = new SqlCategoria();
    private SqlConta sqlCont = new SqlConta();
    private SqlPagamento sqlPag = new SqlPagamento();
	
	public SqlLancamento() {
		try {
			createTable();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createTable() throws ClassNotFoundException{		
		final String sql = "CREATE TABLE IF NOT EXISTS 'lancamento' ('id'	INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "'id_categoria'	INTEGER NOT NULL, "
				+ "'id_pagamento'	INTEGER NOT NULL,"
				+ "'id_conta'	INTEGER NOT NULL, "
				+ "'valor'	REAL NOT NULL, "
				+ "'tipo' TEXT NOT NULL,"
				+ "FOREIGN KEY(id_categoria) REFERENCES categoria(id),"
				+ "FOREIGN KEY(id_pagamento) REFERENCES pagamento(id),"
				+ "FOREIGN KEY(id_conta) REFERENCES conta(id));";
		try (Connection conn = Conecta.conectar();
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}
	
	public void insert(Lancamento lancamento) throws ClassNotFoundException{
		final String sql = "INSERT INTO lancamento VALUES(?,?,?,?,?,?)";
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);) {
        	ps.setString(1, null);
        	ps.setInt(2, lancamento.getCategoria().getId());
        	ps.setInt(3, lancamento.getTipoPagamento().getId());
        	ps.setInt(4 ,lancamento.getConta().getId());
        	ps.setDouble(5, lancamento.getValor());
        	ps.setString(6, lancamento.getTipo().toString());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
        	e.printStackTrace();
        }
	}
	
	public List<Lancamento> selctAll() throws ClassNotFoundException{
		final String sql = "SELECT * FROM lancamento";
        List<Lancamento> lancamentos = new ArrayList<>();
        try (Connection conn = Conecta.conectar();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
            	Lancamento lancamento = new Lancamento(rs.getInt("id"), sqlCat.selectItem(rs.getInt("id_categoria")), sqlPag.selectItem(rs.getInt("id_pagamento")),sqlCont.selectItem(rs.getInt("id_conta")), rs.getDouble("valor"), Tipo.valueOf(rs.getString("tipo")));
                lancamentos.add(lancamento);
            }
            stmt.close();
            rs.close();
            conn.close();
            return lancamentos;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}
	
	
	public Lancamento selectItem(int id) throws ClassNotFoundException{
		String sql = "SELECT * FROM lancamento WHERE id == ?";
		Lancamento lancamento = new Lancamento();
		try (Connection conn = Conecta.conectar();PreparedStatement pstmt  = conn.prepareStatement(sql)){
			pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
			while (rs.next()) {
				lancamento.setId(rs.getInt("id"));
				lancamento.setCategoria(sqlCat.selectItem(rs.getInt("id_categoria")));
				lancamento.setTipoPagamento(sqlPag.selectItem(rs.getInt("id_pagamento")));
				lancamento.setConta(sqlCont.selectItem(rs.getInt("id_conta")));
				lancamento.setValor(rs.getDouble("valor"));
				lancamento.setTipo(Tipo.valueOf(rs.getString("tipo")));				
				
			}
			pstmt.close();
            rs.close();
			conn.close();
			return lancamento;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public void delete(int id) throws ClassNotFoundException{		
		final String sql = "DELETE FROM lancamento WHERE id = ?";		 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){
            ps.setInt(1, id);
            ps.executeUpdate();
            con.close();
            ps.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public void update(Lancamento lancamento) throws ClassNotFoundException{		
		final String sql = "UPDATE lancamento SET id_categoria = ?, id_pagamento = ?, id_conta = ?, valor = ?, tipo = ? WHERE id == ?";
 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){ 
            ps.setInt(1, lancamento.getCategoria().getId());
            ps.setInt(2, lancamento.getTipoPagamento().getId());
            ps.setInt(3, lancamento.getConta().getId());
            ps.setDouble(4, lancamento.getValor());
            ps.setString(5, lancamento.getTipo().toString());
            ps.setInt(6, lancamento.getId());
            ps.executeUpdate();
            con.close();
            ps.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	

	public Double totalDebito() throws ClassNotFoundException{
		final String sql = "SELECT sum(valor) FROM lancamento where tipo = 'DEBITO'";
        try (Connection conn = Conecta.conectar();
        	PreparedStatement pstmt  = conn.prepareStatement(sql);){
			ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
            	return rs.getDouble("sum(valor)");
            }
            pstmt.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}

	public Double totalCredito() throws ClassNotFoundException{
		final String sql = "SELECT sum(valor) FROM lancamento where tipo = 'CREDITO'";
        try (Connection conn = Conecta.conectar();
        	PreparedStatement pstmt  = conn.prepareStatement(sql);){
			ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
            	return rs.getDouble("sum(valor)");
            }
            pstmt.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}
	
	
	
	public Double totalDebitoPorConta(int id) throws ClassNotFoundException{
		final String sql = "SELECT sum(valor) FROM lancamento where id_conta = ? AND tipo = 'DEBITO'";
        try (Connection conn = Conecta.conectar();
        	PreparedStatement pstmt  = conn.prepareStatement(sql);){
        	pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
            	return rs.getDouble("sum(valor)");
            }
            pstmt.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}

	public Double totalCreditoPorConta(int id) throws ClassNotFoundException{
		final String sql = "SELECT sum(valor) FROM lancamento where id_conta = ? AND tipo = 'CREDITO'";
        try (Connection conn = Conecta.conectar();
        	PreparedStatement pstmt  = conn.prepareStatement(sql);){
        	pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
            	return rs.getDouble("sum(valor)");
            }
            pstmt.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}

	public Double totalDebitoPorCategoria(int id) throws ClassNotFoundException{
		final String sql = "SELECT sum(valor) FROM lancamento where id_categoria = ? AND tipo = 'DEBITO'";
        try (Connection conn = Conecta.conectar();
        	PreparedStatement pstmt  = conn.prepareStatement(sql);){
        	pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
            	return rs.getDouble("sum(valor)");
            }
            pstmt.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}
	
	public Double totalCreditoPorCategoria (int id) throws ClassNotFoundException{
		final String sql = "SELECT sum(valor) FROM lancamento where id_categoria = ? AND tipo = 'CREDITO'";
        try (Connection conn = Conecta.conectar();
        	PreparedStatement pstmt  = conn.prepareStatement(sql);){
        	pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
            	return rs.getDouble("sum(valor)");
            }
            pstmt.close();
            rs.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}

	
}
