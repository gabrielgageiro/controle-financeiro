package lancamento;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import banco.Conecta;
import categoria.Categoria;
import conta.Conta;
import pagamento.Pagamento;
import utils.Tipo;

public class Lancamento {
	
	private int id;
	private Categoria categoria;
	private Pagamento tipoPagamento;
	private Conta conta;
	private Double valor;
	private Tipo tipo;
	
	public Lancamento() {
		
	}
	
	public Lancamento(int id, Categoria categoria, Pagamento tipoPagamento, Conta conta, Double valor, Tipo tipo) {
		this.id = id;
		this.categoria = categoria;
		this.tipoPagamento = tipoPagamento;
		this.conta = conta;
		this.valor = valor;
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Pagamento getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(Pagamento tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
}
