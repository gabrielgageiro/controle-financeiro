package utils;

public enum Tipo {
	
	DEBITO("D�bito"),
	CREDITO("Cr�dito");
	private String descricao;

	Tipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
