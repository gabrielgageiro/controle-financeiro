package conta;
import java.math.BigDecimal;

public class Conta {
		
	private int id;
	private String nome;
	private Double saldo;
	private String titular;
	
	public Conta() {
		
	}
	public Conta(int id, String nome, Double saldo, String titular) {
		this.id = id;
		this.nome = nome;
		this.saldo = saldo;
		this.titular = titular;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	
	

}
