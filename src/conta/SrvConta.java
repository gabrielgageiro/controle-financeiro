package conta;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import categoria.SqlCategoria;

@WebServlet(name="Conta", urlPatterns = {"/conta"})
public class SrvConta extends HttpServlet{

	private static final long serialVersionUID = 5L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		String[] checks = req.getParameterValues("checkbox");
		SqlConta sql = new SqlConta();
		if(req.getParameter("id") != null && !req.getParameter("id").equals("undefined")){
			RequestDispatcher view = req.getRequestDispatcher("/conta/cadastroConta.jsp");
			view.forward(req, resp);
		}else{
			for(String check :checks){
				try {
					sql.delete(Integer.parseInt(check));
					
				} catch (NumberFormatException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					resp.getWriter().println("N�o Deu");
				}
				
			}
			RequestDispatcher view = req.getRequestDispatcher("/conta/conta.jsp");
			view.forward(req, resp);
		}
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/conta/conta.jsp");
		view.forward(req, resp);
		
	}
	
	
}
