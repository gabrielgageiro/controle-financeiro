package conta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import banco.Conecta;

public class SqlConta {
	
	public SqlConta() {
		try {
			createTable();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createTable() throws ClassNotFoundException{		
		final String sql = "CREATE TABLE IF NOT EXISTS 'conta' ('id'	INTEGER PRIMARY KEY AUTOINCREMENT,'nome'	TEXT NOT NULL, 'saldo'	REAL NOT NULL,'titular'	TEXT NOT NULL);";
		try (Connection conn = Conecta.conectar();
                Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
	}
	
	public void insert(Conta conta) throws ClassNotFoundException{
		final String sql = "INSERT INTO conta VALUES(?,?,?,?)";
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);) {
        	ps.setString(1, null);
        	ps.setString(2, conta.getNome());
        	ps.setDouble(3, conta.getSaldo());
        	ps.setString(4, conta.getTitular());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public List<Conta> selctAll() throws ClassNotFoundException{
		final String sql = "SELECT id, nome,saldo,titular  FROM conta";
        List<Conta> contas = new ArrayList<>();
        try (Connection conn = Conecta.conectar();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
            	Conta conta = new Conta(rs.getInt("id"), rs.getString("nome"), rs.getDouble("saldo"), rs.getString("titular"));
                contas.add(conta);
            }
            stmt.close();
            rs.close();
            conn.close();
            return contas;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
 		return null;   
	}
	
	
	public Conta selectItem(int id) throws ClassNotFoundException{
		String sql = "SELECT id, nome,saldo,titular  FROM conta WHERE id == ?";
		Conta conta = new Conta();
		try (Connection conn = Conecta.conectar();PreparedStatement pstmt  = conn.prepareStatement(sql)){
			pstmt.setDouble(1,id);
			ResultSet rs  = pstmt.executeQuery();
			while (rs.next()) {
				conta.setId(rs.getInt("id"));
				conta.setNome(rs.getString("nome"));
				conta.setSaldo(rs.getDouble("saldo"));
				conta.setTitular(rs.getString("titular"));
			}
			pstmt.close();
            rs.close();
			conn.close();
			return conta;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public void delete(int id) throws ClassNotFoundException{		
		final String sql = "DELETE FROM conta WHERE id = ?";		 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){
            ps.setInt(1, id);
            ps.executeUpdate(); 
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
	
	public void update(Conta conta) throws ClassNotFoundException{		
		final String sql = "UPDATE conta SET nome = ?, saldo = ?, titular = ? WHERE id = ?";
 
        try (Connection con = Conecta.conectar(); PreparedStatement ps = con.prepareStatement(sql);){ 
            ps.setString(1, conta.getNome());
            ps.setDouble(2, conta.getSaldo());
            ps.setString(3, conta.getTitular());
            ps.setInt(4, conta.getId());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
