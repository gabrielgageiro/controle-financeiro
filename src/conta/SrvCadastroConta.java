package conta;
import java.io.IOException;
import java.math.BigDecimal;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import categoria.Categoria;
import categoria.SqlCategoria;

@WebServlet(name="CadastroConta", urlPatterns = {"/cadastroConta"})
public class SrvCadastroConta extends HttpServlet{

	private static final long serialVersionUID = 4L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		Conta conta = new Conta();
		SqlConta sql = new SqlConta();
		
		conta.setNome(req.getParameter("nome"));
		conta.setSaldo(new Double(req.getParameter("saldo")));
		conta.setTitular(req.getParameter("titular"));
		try {
			if(req.getParameter("id") != null && req.getParameter("id") != ""){
				conta.setId(Integer.parseInt(req.getParameter("id")));				
				sql.update(conta);
				resp.sendRedirect("./conta");
			}else{
				sql.insert(conta);
				resp.sendRedirect("./conta");
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.getWriter().println("N�o Deu");
		}
		
	}
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		RequestDispatcher view = req.getRequestDispatcher("/conta/cadastroConta.jsp");
		view.forward(req, resp);
		
	}
	
	
}
