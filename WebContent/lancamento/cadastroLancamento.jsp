<%@page import="lancamento.Lancamento"%>
<%@page import="conta.Conta"%>
<%@page import="conta.SqlConta"%>
<%@page import="pagamento.Pagamento"%>
<%@page import="pagamento.SqlPagamento"%>
<%@page import="lancamento.SqlLancamento"%>
<%@page import="categoria.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="categoria.SqlCategoria"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
SqlCategoria sql = new SqlCategoria(); 		
List<Categoria> categoria = new ArrayList<Categoria>();
categoria = sql.selctAll();

SqlPagamento sqlPag = new SqlPagamento(); 		
List<Pagamento> pagamentos = new ArrayList<Pagamento>();
pagamentos = sqlPag.selctAll();

SqlConta sqlConta = new SqlConta();
List<Conta> contas = new ArrayList<Conta>();
contas = sqlConta.selctAll();

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<meta charset="ISO-8859-1">
<title>Cadastro de Lan�amentos</title>
</head>
<body>

<nav class="navbar navbar-dark navbar-expand-sm navbar-light w3-blue-grey fixed-top">

<div aria-live="polite" aria-atomic="true">
  <div style="position: absolute; top: 10px; right: 10px;">
  
<% if(categoria.size() == 0){ %>
	<div aria-live="polite" aria-atomic="true">
  		<div class="toast bg-danger text-white" data-autohide="false">
    		<div class="toast-header">
      			<strong class="mr-auto">Alerta!</strong>
      			<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        			<span aria-hidden="true">&times;</span>
      			</button>
    		</div>
    		<div class="toast-body">
      			Nenhuma Categoria Cadastrada!
		    </div>
  		</div>
	</div>
<script>
$(document).ready(function(){
  $('.toast').toast('show');
});
</script>
<%} %>

<% if(pagamentos.size() == 0){ %>
	<div aria-live="polite" aria-atomic="true">
  		<div class="toast bg-danger text-white" data-autohide="false">
    		<div class="toast-header">
      			<strong class="mr-auto">Alerta!</strong>
      			<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        			<span aria-hidden="true">&times;</span>
      			</button>
    		</div>
    		<div class="toast-body">
      			Nenhuma Tipo de Pagamento Cadastrado!
		    </div>
  		</div>
	</div>
<script>
$(document).ready(function(){
  $('.toast').toast('show');
});
</script>
<%} %>

<% if(contas.size() == 0){ %>
	<div aria-live="polite" aria-atomic="true">
  		<div class="toast bg-danger text-white" data-autohide="false">
    		<div class="toast-header">
      			<strong class="mr-auto">Alerta!</strong>
      			<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        			<span aria-hidden="true">&times;</span>
      			</button>
    		</div>
    		<div class="toast-body">
      			Nenhuma Conta Cadastrada!
		    </div>
  		</div>
	</div>
<script>
$(document).ready(function(){
  $('.toast').toast('show');
});
</script>
<%} %>
	</div>
</div>

  <div class="collapse navbar-collapse" id="navbarNav">
  <a class="navbar-brand" href="./">
          <img src="https://images.vexels.com/media/users/3/128340/isolated/preview/8b26c4ab0c49dfe0ca378ea3d816de57---cone-de-carteira-aberta-by-vexels.png" height="40" width="40" style="margin:-13px;margin-left:0px;">
    </a>
    <ul class="navbar-nav align-items-center">
      <li class="nav-item">
        <a class="nav-link" href="./">Inicio</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./conta">Contas</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./categoria">Categorias</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./pagamento">Pagamentos</a>
      </li> 
      <li class="nav-item active">
        <a class="nav-link" href="./lancamento">Lan�amentos</a>
      </li>
    </ul>
  </div>
</nav>


<br/>
<div class="w3-container w3-text-grey" style="margin-top:50px">
  <h3>Cadastro de Lan�amento</h3>
</div>
<br/>


<form action="./cadastroLancamento" method="post" class="w3-container">
<% SqlLancamento sqlLanc = new SqlLancamento(); 
	String id = request.getParameter("id");
	Lancamento l = null;
			if(id != null){
				l = sqlLanc.selectItem(Integer.parseInt(id));
			}
%>

	<!-- Criar arquivo JSP para listar as categorias  -->
	<input type="hidden"  name="id" value=<%= (l == null) ? "": l.getId()%>>
	<select onchange="this.className = 'w3-select w3-input'" id="categoria" class="w3-select w3-input w3-text-grey" name="categoria" required>
	<option hidden value="">Categoria</option>
	<%	
  		for(int i = 0; i < categoria.size(); i++){
  			if(l != null){
  			if(categoria.get(i).getId() == l.getCategoria().getId()){
  	%>			
  		<option class="w3-text-black" selected="selected" value="<%= categoria.get(i).getId()%>"><%= categoria.get(i).getNome()%></option>
  		<script>
  		var element = document.getElementById("categoria");
  		element.className = 'w3-select w3-input';</script>
  		<%}}else{ %>
  		<option class="w3-text-black" value="<%= categoria.get(i).getId()%>"><%= categoria.get(i).getNome()%></option>
  		
  	<% }}%>
	</select>
	<br/>
	
	<!-- Criar arquivo JSP para listar os tipos de pagamentos  -->
	<select onchange="this.className = 'w3-select w3-input'" id="pagamento" class="w3-select w3-input w3-text-grey" name="tipoPagamento" required>
	<option hidden value="">Tipo de Pagamento</option>
  	<%	
		
  		for(int i = 0; i < pagamentos.size(); i++){
  			if(l != null){
  			if(pagamentos.get(i).getId() == l.getTipoPagamento().getId()){
  	%>
  		<option class="w3-text-black" selected="selected" value="<%= pagamentos.get(i).getId()%>"><%= pagamentos.get(i).getTipo()%></option>
  		<script>
  		var element = document.getElementById("pagamento");
  		element.className = 'w3-select w3-input';</script>
  		<%}}else{ %>
  		<option class="w3-text-black" value="<%= pagamentos.get(i).getId()%>"><%= pagamentos.get(i).getTipo()%></option>
	<% }} %>
	</select>
	<br/>

	<!-- Criar arquivo JSP para listar as contas  -->

	<select onchange="this.className = 'w3-select w3-input'" id="conta" class="w3-select w3-input w3-text-grey" name="conta" required>
	<option hidden value="">Conta</option>
  	<% 	
		for(int i = 0; i < contas.size(); i++){
  			if(l != null){
			if(contas.get(i).getId()== l.getConta().getId()){
  	%>
  		<option class="w3-text-black" selected="selected"  value="<%= contas.get(i).getId()%>"><%= contas.get(i).getNome() %></option>
  		<script>
  		var element = document.getElementById("conta");
  		element.className = 'w3-select w3-input';</script>
  		<%}}else{ %>
  		<option class="w3-text-black" value="<%= contas.get(i).getId()%>"><%= contas.get(i).getNome() %></option>
  	<% }} %>
	</select>
	<br/>

	<input class="w3-input" type="number" step="0.01" name="valor" required placeholder="Valor" value=<%= (l == null) ? "": l.getValor()%>>
	<br/>

<!-- Criar arquivo JSP para trazer o valor da ENUM  -->
	<div class="w3-container w3-text-grey" style="margin-buttom:-20px;margin-left:-15px;">
  		<h6>Tipo de Lan�amento</h6>
	</div>
	<div>
	<%if(l != null){
		if(l.getTipo().getDescricao().equals("Cr�dito")){%>
		<input class="w3-radio" type="radio" name="tipo" value="DEBITO">
		<label>D�bito</label>
		<input class="w3-radio" type="radio" name="tipo" value="CREDITO" checked="checked">
		<label>Cr�dito</label>
		<%}else if(l.getTipo().getDescricao().equals("D�bito")){%>
		<input class="w3-radio" type="radio" name="tipo" value="DEBITO" checked="checked">
		<label>D�bito</label>
		<input class="w3-radio" type="radio" name="tipo" value="CREDITO">
		<label>Cr�dito</label>
		<%}}else{%>
		<input class="w3-radio" type="radio" name="tipo" value="DEBITO" required>
		<label>D�bito</label>
		<input class="w3-radio" type="radio" name="tipo" value="CREDITO">
		<label>Cr�dito</label>
		<%}%>
	</div>
	<br/>

	<input type="submit" value="Lan�ar" class="w3-btn w3-blue-grey">
	<button type="button" class="w3-btn w3-blue-grey" onclick="location.href='./lancamento';">Cancelar</button>
</form>

</body>
</html>