<%@page import="pagamento.Pagamento"%>
<%@page import="pagamento.SqlPagamento"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Cadastro de Formas Pagamento</title>
</head>
<body>

<nav class="navbar navbar-dark navbar-expand-sm navbar-light w3-blue-grey">
  <div class="collapse navbar-collapse" id="navbarNav">
  <a class="navbar-brand" href="./">
          <img src="https://images.vexels.com/media/users/3/128340/isolated/preview/8b26c4ab0c49dfe0ca378ea3d816de57---cone-de-carteira-aberta-by-vexels.png" height="40" width="40" style="margin:-13px;margin-left:0px;">
    </a>
    <ul class="navbar-nav align-items-center">
      <li class="nav-item">
        <a class="nav-link" href="./">Inicio</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./conta">Contas</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./categoria">Categorias</a>
      </li>      
      <li class="nav-item active">
        <a class="nav-link" href="./pagamento">Pagamentos</a>
      </li> 
      <li class="nav-item ">
        <a class="nav-link" href="./lancamento">Lanšamentos</a>
      </li>
    </ul>
  </div>
</nav>

<br/>
<div class="w3-container w3-text-grey">
  <h3>Cadastro de Formas Pagamento</h3>
</div>
<br/>



<form action="cadastroPagamento" method="post" class="w3-container">
<% SqlPagamento sql = new SqlPagamento(); 
	String id = request.getParameter("id");
	Pagamento p = new Pagamento();
			if(id != null){
				p = sql.selectItem(Integer.parseInt(id));
			}
	
%>
	<input type="hidden"  name="id" value=<%= (p.getId() == 0) ? "": p.getId()%>>
	<input class="w3-input" type="text" name="formaPagamento" required placeholder="Nome da Forma de Pagamento" value="<%= (p.getTipo() != null) ? p.getTipo():"" %>">
	<br/>
	<input type="submit" value="Salvar" class="w3-btn w3-blue-grey">
	<button type="button" class="w3-btn w3-blue-grey" onclick="location.href='./pagamento';">Cancelar</button>
</form>

</body>
</html>