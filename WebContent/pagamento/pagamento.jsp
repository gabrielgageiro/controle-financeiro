<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="pagamento.SqlPagamento"%>
<%@page import="pagamento.Pagamento"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
	document.getElementById("excluir").disabled = true;
    function func(id){
    	document.getElementById("id").value = id;
    	document.getElementById("edit").submit();
    }
    
	function valida(){
		const elementos = document.getElementsByName("checkbox")
    		for(var i = 0; i < elementos.length; i++){
	    		if(elementos[i].checked){
	    			document.getElementById("excluir").disabled = false;
	    			i = elementos.length;;
    				}
	    		else{
	    			document.getElementById("excluir").disabled = true;
	    		}
    	}
    }
    
</script>
<style>
.row{
	margin-right:0px !important;
	margin-left:0px !important;
}
</style>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listagem de Fomas de Pagamento</title>
</head>
<body>

<nav class="navbar navbar-dark navbar-expand-sm navbar-light w3-blue-grey fixed-top">
  <div class="collapse navbar-collapse" id="navbarNav">
  <a class="navbar-brand" href="./">
          <img src="https://images.vexels.com/media/users/3/128340/isolated/preview/8b26c4ab0c49dfe0ca378ea3d816de57---cone-de-carteira-aberta-by-vexels.png" height="40" width="40" style="margin:-13px;margin-left:0px;">
    </a>
    <ul class="navbar-nav align-items-center">
      <li class="nav-item">
        <a class="nav-link" href="./">Inicio</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./conta">Contas</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./categoria">Categorias</a>
      </li>      
      <li class="nav-item  active">
        <a class="nav-link" href="./pagamento">Pagamentos</a>
      </li> 
      <li class="nav-item ">
        <a class="nav-link" href="./lancamento">Lanšamentos</a>
      </li>
    </ul>
  </div>
</nav>

<br/>
<div class="w3-container w3-text-grey" style="margin-top:50px">
  <h3>Listagem de Formas de Pagamento</h3>
</div>
<br/>

<div style="margin:20px;">
	<nav class="nav w3-blue-grey">
  		<button type="button" onclick="location.href='./cadastroPagamento';" class="btn btn-outline-light" style="border:none; border-radius: 0px 0px 0px 0px; margin:1px;">Adicionar</button>
  		<button type="button" id="excluir" disabled onclick="func()" class="btn btn-outline-light" style="border:none; border-radius: 0px 0px 0px 0px; margin:1px;">Excluir</button>
	</nav>
	<form action="./pagamento" method="post" id="edit">
  	<div class="row">
  		<div class="col-1 border bg-light font-weight-bold"></div>
  		<div class="col-1 border bg-light font-weight-bold">Editar</div>
    	<div class="col border bg-light font-weight-bold">ID</div>
    	<div class="col border bg-light font-weight-bold">Nome</div>
  	</div>
  	
  	<!-- Aqui vai o JSP paa trazer os itens -->
  		<% SqlPagamento sql = new SqlPagamento();
  		List<Pagamento> pagamentos = new ArrayList<Pagamento>();
  			pagamentos = sql.selctAll();
  			for(int i = 0; i < pagamentos.size(); i++){ 
  		%>
  	<div class="row">
  		<div class="col-1 border bg-light font-weight-bold text-center"><input  style="margin-top:5px" name="checkbox" value="<%= pagamentos.get(i).getId() %>" type="checkbox" onchange="valida()"> </div>
  		<div class="col-1 border bg-light font-weight-bold text-center"><a style="color:#757575;cursor: pointer;" onmouseover="this.style.opacity=0.5;" onmouseout="this.style.opacity=1;" onclick="func(<%= pagamentos.get(i).getId()%>);"><i class="material-icons">edit</i></a></div>
    	<div class="col border bg-light "><%= pagamentos.get(i).getId()%></div>
    	<div class="col border bg-light "><%= pagamentos.get(i).getTipo()%></div>
  	</div>
  	<% }%>
  	<input type="hidden" id="id" name="id"/>
  	</form>
</div>


</body>
</html>