<%@page import="categoria.Categoria"%>
<%@page import="categoria.SqlCategoria"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Cadastro de Categorias</title>
</head>
<body>

<nav class="navbar navbar-dark navbar-expand-sm navbar-light w3-blue-grey fixed-top">
  <div class="collapse navbar-collapse" id="navbarNav">
  <a class="navbar-brand" href="./">
          <img src="https://images.vexels.com/media/users/3/128340/isolated/preview/8b26c4ab0c49dfe0ca378ea3d816de57---cone-de-carteira-aberta-by-vexels.png" height="40" width="40" style="margin:-13px;margin-left:0px;">
    </a>
    <ul class="navbar-nav align-items-center">    
      <li class="nav-item">
        <a class="nav-link" href="./">Inicio</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./conta">Contas</a>
      </li>      
      <li class="nav-item  active">
        <a class="nav-link" href="./categoria">Categorias</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./pagamento">Pagamentos</a>
      </li> 
      <li class="nav-item ">
        <a class="nav-link" href="./lancamento">Lanšamentos</a>
      </li>
    </ul>
  </div>
</nav>

<br/>
<div class="w3-container w3-text-grey" style="margin-top:50px">
  <h3>Cadastro de Categorias</h3>
</div>
<br/>

<form action="./cadastroCategoria" method="post" class="w3-container">
<% SqlCategoria sql = new SqlCategoria(); 
	String id = request.getParameter("id");
	Categoria c = new Categoria();
			if(id != null){
				c = sql.selectItem(Integer.parseInt(id));
			}
	
%>
	<input type="hidden"  name="id" value=<%= (c.getId() == 0) ? "": c.getId()%>>
	<input class="w3-input" type="text" name="categoria" required placeholder="Nome da Categoria" value="<%= (c.getNome() != null) ? c.getNome():"" %>">
	<br/>
	<input type="submit" value="Salvar" class="w3-btn w3-blue-grey">
	<button type="button" class="w3-btn w3-blue-grey" onclick="location.href='./categoria';">Cancelar</button>
</form>



</body>
</html>