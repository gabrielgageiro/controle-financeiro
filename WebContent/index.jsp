<%@page import="categoria.SqlCategoria"%>
<%@page import="categoria.Categoria"%>
<%@page import="lancamento.SqlLancamento"%>
<%@page import="lancamento.Lancamento"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.List"%>
<%@page import="conta.SqlConta"%>
<%@page import="java.util.ArrayList"%>
<%@page import="conta.Conta"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Inicio</title>
</head>
<body>

<nav class="navbar navbar-dark navbar-expand-sm navbar-light w3-blue-grey fixed-top">
  <div class="collapse navbar-collapse" id="navbarNav">
  <a class="navbar-brand" href="#">
          <img src="https://images.vexels.com/media/users/3/128340/isolated/preview/8b26c4ab0c49dfe0ca378ea3d816de57---cone-de-carteira-aberta-by-vexels.png" height="40" width="40" style="margin:-13px;margin-left:0px;">
    </a>
    <ul class="navbar-nav align-items-center">
      <li class="nav-item">
        <a class="nav-link active" href="./">Inicio</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./conta">Contas</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./categoria">Categorias</a>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="./pagamento">Pagamentos</a>
      </li> 
      <li class="nav-item ">
        <a class="nav-link" href="./lancamento">Lan�amentos</a>
      </li>
    </ul>
  </div>
</nav>


<br/>
<div class="w3-container w3-text-grey" style="margin-top:50px">
  <h3>Dashboard</h3>
</div>
<br/>

<div id="gasto" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<br/>
<div id="gastoPorConta" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<br/>
<div id="gastoPorCategoria" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<br/>

<%  List<Conta> contas = new ArrayList();
	SqlConta sqlCont = new SqlConta();
	contas = sqlCont.selctAll();
	
	List<Lancamento> lancamentos = new ArrayList();
	SqlLancamento sqlLanca = new SqlLancamento();
	lancamentos = sqlLanca.selctAll();
	
	
	List<Categoria> categorias = new ArrayList();
	SqlCategoria sqlCat = new SqlCategoria();
	categorias = sqlCat.selctAll();
	
	
%>
<script type="text/javascript">

Highcharts.chart('gasto', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafico de Gastos '
    },
    subtitle: {
        text: 'Valores referente aos total de gastos'
    },
    xAxis: {
        categories: [
		"Gastos"
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Valor (R$)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>R$ {point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: .2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'D�bito',
        data: [
				<%= sqlLanca.totalDebito() %>
               ]

    }, {
        name: 'Cr�dito',
        data: [
				<%= sqlLanca.totalCredito() %>               
               ]

    }],
    navigation: {
    	buttonOptions: {
    	  enabled: false
    	  }
    	 },
    colors: ['#607D8B', '#757575']
});






Highcharts.chart('gastoPorConta', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafico de Gastos por Conta'
    },
    subtitle: {
        text: 'Valores referente aos total de gastos por conta'
    },
    xAxis: {
        categories: [
		<% for(int i=0;i<contas.size(); i++){%>
			"<%= contas.get(i).getNome()%>",
		<%}%>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Valor (R$)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>R$ {point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: .2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'D�bito',
        data: [
				<% for(int i=0;i<contas.size(); i++){%>
					<%= sqlLanca.totalDebitoPorConta(contas.get(i).getId())%>,
				<%}%>
               ]

    }, {
        name: 'Cr�dito',
        data: [
				<% for(int i=0;i<contas.size(); i++){%>
					<%= sqlLanca.totalCreditoPorConta(contas.get(i).getId())%>,
				<%}%>
               
               ]

    }],
    navigation: {
    	buttonOptions: {
    	  enabled: false
    	  }
    	 },
    colors: ['#607D8B', '#757575']
});








Highcharts.chart('gastoPorCategoria', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafico de Gastos por Categoria'
    },
    subtitle: {
        text: 'Valores referente aos total de gastos por categoria'
    },
    xAxis: {
        categories: [
		<% for(int i=0;i<categorias.size(); i++){%>
			"<%= categorias.get(i).getNome()%>",
		<%}%>
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Valor (R$)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>R$ {point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: .2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'D�bito',
        data: [
				<% for(int i=0;i<categorias.size(); i++){%>
					<%= sqlLanca.totalDebitoPorCategoria(categorias.get(i).getId()) %>,
				<%}%>
               ]

    }, {
        name: 'Cr�dito',
        data: [
				<% for(int i=0;i<categorias.size(); i++){%>
					<%= sqlLanca.totalCreditoPorCategoria(categorias.get(i).getId())%>,
				<%}%>
               
               ]

    }],
    navigation: {
    	buttonOptions: {
    	  enabled: false
    	  }
    	 },
    colors: ['#607D8B', '#757575']
});

</script>
</body>
</html>